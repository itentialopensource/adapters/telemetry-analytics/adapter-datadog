# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Datadog System. The API that was used to build the adapter for Datadog is usually available in the report directory of this adapter. The adapter utilizes the Datadog API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The datadog adapter from Itential is used to integrate the Itential Automation Platform (IAP) with datadog. With this adapter you have the ability to determine performance metrics as well as do Event monitoring for infrastructure and cloud services. With this adapter you have the ability to perform operations such as:

- Get Event

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
