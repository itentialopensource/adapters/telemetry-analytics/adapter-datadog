# Datadog

Vendor: Datadog
Homepage: https://www.datadoghq.com/

Product: Datadog
Product Page: https://www.datadoghq.com/

## Introduction
We classify datadog into the Telemetry Analytics domain as datadog is monitoring and analytics tool for information technology (IT) and DevOps teams.

## Why Integrate
The datadog adapter from Itential is used to integrate the Itential Automation Platform (IAP) with datadog. With this adapter you have the ability to determine performance metrics as well as do Event monitoring for infrastructure and cloud services. With this adapter you have the ability to perform operations such as:

- Get Event

## Additional Product Documentation
The [API documents for datadog](https://docs.datadoghq.com/api/latest/)